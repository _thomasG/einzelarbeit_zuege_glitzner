package einzelArbeit_zuege_glitzner;

import java.util.ArrayList;
import einzelArbeit_zuege_glitzner.Utility;

public class Zug {

	private final String bezeichnung;
	private Lokomotive lokomotive;
	private ArrayList<Waggon> angehaengt = new ArrayList<Waggon>();
	private static ArrayList<Zug> alleZuege = new ArrayList<Zug>();

	public Zug(String bezeichnung, Lokomotive lokomotive, Waggon... waggons) {
		super();
		for (Zug Zug : alleZuege) {
			if (Zug.bezeichnung.equalsIgnoreCase(bezeichnung)) {
				throw new IllegalArgumentException("Bezeichnung bereits in Verwendung");
			}
		}
		if (Utility.isEmptyString(bezeichnung)) {
			throw new IllegalArgumentException("Bezeichnung darf nicht leer sein");
		}
		this.bezeichnung = bezeichnung;

		for (Zug Zug : alleZuege) {
			if (Zug.lokomotive.equals(this.lokomotive)) {
				throw new IllegalArgumentException("Lokomotive bereits in Verwendung");
			}
		}
		this.lokomotive = lokomotive;
		anhaengen(waggons);
		if (!gewichtsTest(this)) {
			this.lokomotive = null;
			throw new IllegalArgumentException("Die angegebene Lokomotive ist nicht stark genug f�r all diese Waggons");
		}
		alleZuege.add(this);
		this.lokomotive.vorneDran(this);
	}

	@Override
	public String toString() {
		return (bezeichnung + " " + (lokomotive + "" + (angehaengt != null ? " " + angehaengt + " " : "]")));
	}

	public void anhaengen(Waggon... waggons) {
		int gesamtLaenge = this.lokomotive.getLaenge();
		for (Waggon waggon : waggons) {
			gesamtLaenge += waggon.getLaenge();
			if (this.angehaengt.size() < 15) {
				if (waggon.getIstFrei() == true) {
					if (gewichtsTest(this)) {
						if (gesamtLaenge <= 150) {
							this.angehaengt.add(waggon);
							waggon.setIstFrei(true);
						} else {
							System.out.println("Maximale Zug-L�nge erreicht");
						}
					} else {
						System.out.println("Maximalgewicht dieses Zuges erreicht");
					}
				} else {
					System.out.println("Der Waggon " + waggon.getID() + " ist derzeit in Benutzung");
				}
			} else {
				System.out.println("Maximale Anzahl von 15 Waggons wurde erreicht");
			}
		}
	}

	public void abhaengen() {
		if (this.angehaengt.size() > 0) {
			this.angehaengt.get(this.angehaengt.size() - 1).setIstFrei(true);
			this.angehaengt.remove(this.angehaengt.size() - 1);
		} else {
			System.out.println("Kein Waggon zum abh�ngen dran.");
		}
	}

	public boolean gewichtsTest(Zug zug) {
		int gesamtGewicht = 0;
		for (Waggon waggon : zug.angehaengt) {
			gesamtGewicht += waggon.getGewicht() + (waggon.getAnzahlSitzplaetze() * 80);
		}
		if (gesamtGewicht <= zug.lokomotive.getZugkraft()) {
			return true;
		}
		return false;
	}

	public void austauschen(Lokomotive lok) {
		if (Lokomotive.sindFrei().contains(lok)) {
			if (gewichtsTest(this)) {
				this.lokomotive.vorneDran(null);
				this.lokomotive = lok;
				this.lokomotive.vorneDran(this);
			} else {
				System.out.println("Die neue Lokomotive ist nicht stark genug um all diese Waggons zu ziehen");
			}
		} else {
			System.out.println("Lokomotive bereits in Verwendung");
		}
	}

	public String getBezeichnung() {
		return bezeichnung;
	}

	public Lokomotive getLokomotive() {
		return lokomotive;
	}

	public ArrayList<Waggon> getAngehaengt() {
		return angehaengt;
	}

}