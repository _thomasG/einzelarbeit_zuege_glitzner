package einzelArbeit_zuege_glitzner;

import java.util.ArrayList;
import einzelArbeit_zuege_glitzner.Utility;

public class Lokfahrer {

	private String name;
	private ArrayList<Lokomotive> berechtigung = new ArrayList<Lokomotive>();

	public Lokfahrer(String name, Lokomotive ausbildungslok) {

		if (Utility.isEmptyString(name)) {
			throw new IllegalArgumentException("Name darf nicht leer sein");
		}
		this.name = name;
		this.berechtigung.add(ausbildungslok);
	}

	@Override
	public String toString() {
		return "Lokfahrer [" + (name != null ? "name=" + name + ", " : "")
				+ (berechtigung != null ? "berechtigung=" + berechtigung : "") + "]";
	}

	public void darfFahren(Lokomotive lok) {
		if (this.berechtigung == null) {
		}
		for (Lokomotive lokomotive : berechtigung) {
			if (lokomotive.getAntriebsart() == lok.getAntriebsart()) {
				System.out.println(
						"Der Lokf�hrer " + this.name + " hat bereits die Berechtigung f�r eine Lok dieses Typs.");
				return;
			}
			berechtigung.add(lok);
			return;
		}
	}

}