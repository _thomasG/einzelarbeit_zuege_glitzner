package einzelArbeit_zuege_glitzner;

import einzelArbeit_zuege_glitzner.Utility;

public class Waggon extends Fahrzeug {

	private int anzahlSitzplaetze;
	private WaggonArt waggonArt;
	private boolean istFrei;

	public Waggon(int gewicht, int laenge, int anzahlSitzplaetze, WaggonArt waggonArt) {
		super(gewicht, laenge);
		if (Utility.isNullInt(anzahlSitzplaetze)) {
			throw new IllegalArgumentException("Anzahl der Sitzpl�tze muss gr��er 0 sein");
		}
		this.anzahlSitzplaetze = anzahlSitzplaetze;
		this.waggonArt = waggonArt;
		this.istFrei = true;
	}

	@Override
	public String toString() {
		return super.toString() + " " + waggonArt + " " + anzahlSitzplaetze + "]";
	}

	public WaggonArt getWaggonArt() {
		return waggonArt;
	}

	public int getAnzahlSitzplaetze() {
		return anzahlSitzplaetze;
	}

	public boolean getIstFrei() {
		return istFrei;
	}

	public void setIstFrei(boolean istFrei) {
		this.istFrei = istFrei;
	}

}