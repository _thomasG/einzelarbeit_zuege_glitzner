package einzelArbeit_zuege_glitzner;

import einzelArbeit_zuege_glitzner.Utility;

public abstract class Fahrzeug {

	protected int ID;
	private int gewicht;
	private int laenge;

	private static int startwert = 10000;

	public Fahrzeug(int gewicht, int laenge) {
		super();
		this.ID = startwert++;
		if (Utility.isNullInt(gewicht)) {
			throw new IllegalArgumentException("Gewicht muss gr��er 0 sein");
		}
		this.gewicht = gewicht; // in kg
		if (Utility.isNullInt(laenge)) {
			throw new IllegalArgumentException("L�nge muss gr��er 0 sein");
		}
		this.laenge = laenge; // in meter
	}

	@Override
	public String toString() {
		return "[" + ID;
	}

	public int getID() {
		return ID;
	}

	public int getGewicht() {
		return gewicht;
	}

	public int getLaenge() {
		return laenge;
	}

}
