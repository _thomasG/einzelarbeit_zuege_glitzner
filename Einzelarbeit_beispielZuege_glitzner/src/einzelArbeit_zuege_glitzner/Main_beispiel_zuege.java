package einzelArbeit_zuege_glitzner;

public class Main_beispiel_zuege {

	public static void main(String[] args) {

		Lokomotive dampflok1 = null;
		dampflok1 = tryCatchLok(dampflok1, 15000, 15, 2500, 25000, AntriebsArt.Dampf);
		Waggon schlafwaggon1 = null;
		schlafwaggon1 = tryCatchWaggon(schlafwaggon1, 3000, 15, 20, WaggonArt.Schlaf);
		Waggon abteilwaggon1 = null;
		abteilwaggon1 = tryCatchWaggon(abteilwaggon1, 3000, 15, 30, WaggonArt.Abteil);
		Waggon speisewaggon1 = new Waggon(3500, 15, 25, WaggonArt.Speise);
		speisewaggon1 = tryCatchWaggon(speisewaggon1, 3500, 15, 25, WaggonArt.Speise);

		System.out.println(dampflok1);
		System.out.println(schlafwaggon1);
		System.out.println(abteilwaggon1);
		System.out.println(speisewaggon1);

		Zug orientExpress = null;
		orientExpress = tryCatchZug(orientExpress, "Orient Express", dampflok1, schlafwaggon1, abteilwaggon1,
				speisewaggon1);

		System.out.println(orientExpress);

		Lokomotive diesellok1 = null;
		diesellok1 = tryCatchLok(diesellok1, 15000, 15, 2500, 50000, AntriebsArt.Diesel);

		Waggon gro�raumwaggon1 = null;
		gro�raumwaggon1 = tryCatchWaggon(gro�raumwaggon1, 4000, 20, 50, WaggonArt.Gro�raum);

		Zug alpenExpress = null;
		alpenExpress = tryCatchZug(alpenExpress, "Alpen Express", diesellok1);
		System.out.println(alpenExpress);

		alpenExpress.anhaengen(gro�raumwaggon1);
		System.out.println(alpenExpress);

		try {
			Lokfahrer hansHuber = new Lokfahrer("Hans Huber", diesellok1);
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}

		Lokomotive.kannZiehen(4000);

		alpenExpress.abhaengen();

		Lokomotive lokomotive1 = null;
		lokomotive1 = tryCatchLok(lokomotive1, 15000, 15, 2500, 25000, AntriebsArt.Dampf);

		Lokomotive lokomotive2 = null;
		lokomotive2 = new Lokomotive(15000, 15, 2500, 25000, AntriebsArt.Dampf);

		Zug Z1 = new Zug("Testzug1", lokomotive1);
		System.out.println();

		System.out.println(Lokomotive.sindFrei());

		Z1.austauschen(lokomotive2);

		System.out.println(Lokomotive.sindFrei());

		System.out.println(lokomotive1);

	}

	private static Zug tryCatchZug(Zug zug, String bezeichnung, Lokomotive lokomotive, Waggon... waggons) {
		try {
			zug = new Zug(bezeichnung, lokomotive, waggons);
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
		return zug;
	}

	private static Lokomotive tryCatchLok(Lokomotive lok, int gewicht, int laenge, int PS, int zugkraft,
			AntriebsArt antriebsart) {
		try {
			lok = new Lokomotive(gewicht, laenge, PS, zugkraft, antriebsart);
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
		return lok;
	}

	private static Waggon tryCatchWaggon(Waggon waggon, int gewicht, int laenge, int anzahlSitzplaetze,
			WaggonArt waggonArt) {
		try {
			waggon = new Waggon(gewicht, laenge, anzahlSitzplaetze, waggonArt);
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
		return waggon;
	}
}