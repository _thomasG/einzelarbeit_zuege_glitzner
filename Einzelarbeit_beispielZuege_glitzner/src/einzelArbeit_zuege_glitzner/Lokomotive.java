package einzelArbeit_zuege_glitzner;

import java.util.ArrayList;
import einzelArbeit_zuege_glitzner.Utility;

public class Lokomotive extends Fahrzeug {

	private int PS;
	private int zugkraft; // in kg
	private AntriebsArt antriebsart;
	private Zug vorneDran;
	private static ArrayList<Lokomotive> alleLokomotiven = new ArrayList<Lokomotive>();

	public Lokomotive(int gewicht, int laenge, int PS, int zugkraft, AntriebsArt antriebsart) {

		super(gewicht, laenge);
		if (Utility.isNullInt(PS)) {
			throw new IllegalArgumentException("Die PS m�ssen gr��er 0 sein");
		}
		this.PS = PS;
		if (Utility.isNullInt(zugkraft)) {
			throw new IllegalArgumentException("Die Zugkraft (in kg) muss gr��er 0 sein");
		}
		this.zugkraft = zugkraft;
		this.antriebsart = antriebsart;
		alleLokomotiven.add(this);
	}

	@Override
	public String toString() {
		return super.toString() + " " + antriebsart + " " + zugkraft + "]";
	}

	public void vorneDran(Zug zug) {
		this.vorneDran = zug;
	}

	public static ArrayList<Lokomotive> sindFrei() {
		ArrayList<Lokomotive> freieLokomotiven = new ArrayList<Lokomotive>();
		for (Lokomotive lok : alleLokomotiven) {
			if (lok.vorneDran == null) {
				freieLokomotiven.add(lok);
			}
		}
		return freieLokomotiven;
	}

	public static void kannZiehen(int gewicht) {
		int vergleich = 2147483647;
		Lokomotive vergleichsLok = null;
		for (Lokomotive lok : sindFrei()) {
			if (lok.zugkraft - gewicht > 0 && lok.zugkraft - gewicht < vergleich) {
				vergleich = (lok.zugkraft - gewicht);
				vergleichsLok = lok;
			}
		}
		if (vergleichsLok == null) {
			System.out.println("Derzeit keine freien Z�ge die dieses Gewicht ziehen k�nnen.");
		} else {
			System.out.println(vergleichsLok);
		}
		return;
	}

	public int getPS() {
		return PS;
	}

	public int getZugkraft() {
		return zugkraft;
	}

	public AntriebsArt getAntriebsart() {
		return antriebsart;
	}

	public static ArrayList<Lokomotive> getAlleLokomotiven() {
		return alleLokomotiven;
	}

	public Zug getVorneDran() {
		return vorneDran;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Lokomotive other = (Lokomotive) obj;
		if (PS != other.PS)
			return false;
		if (antriebsart != other.antriebsart)
			return false;
		if (vorneDran == null) {
			if (other.vorneDran != null)
				return false;
		} else if (!vorneDran.equals(other.vorneDran))
			return false;
		if (zugkraft != other.zugkraft)
			return false;
		return true;
	}

}